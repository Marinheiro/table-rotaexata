# -*- coding: utf-8 -*-
from src.info import teste

from src.table import gerar_tabela

header = 'src/header.html'
footer = 'src/footer.html'

"""
Gera tabela de acordo com os arquivos header.html e footer.html.
A tabela é gerada em html e concatenada entre header e footer.
"""
with open('index.html', "w") as index:
    header = open(header, "r")
    header = header.readlines()
    footer = open(footer, "r")
    footer = footer.readlines()
    table = gerar_tabela(teste)
    index.writelines(header)
    index.write(table)
    index.writelines(footer)
    
