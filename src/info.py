# -*- coding: utf-8 -*-

from pprint import pprint
'''
	A entrada em formato array foi mudada para dicionário
	em python pois em python a forma que se indexa algo por
 	nome (string) é utilizando os dicionários.
'''
teste = []
# Declaração do produto 1
produto_0 = {}
produto_0['nome'] = "Produto 1"
produto_0['descricao'] = "Descrição do Produto 1"
produto_0['valor'] = 50
produto_0['opcionais'] = ["Opcional 1",
                          "Opcional 2",
                          "Opcional 3"]
teste.append(produto_0)

# Declaração do produto 2
produto_1 = {}
produto_1['nome'] = "Produto 2"
produto_1['descricao'] = "Descrição do Produto 2"
produto_1['valor'] = 75
produto_1['opcionais'] = ["Opcional 1",
                          "Opcional 2",
                          "Opcional 3",
                          "Opcional 4"]
teste.append(produto_1)

# Declaração do produto 3
produto_2 = {}
produto_2['nome'] = "Produto 3"
produto_2['descricao'] = "Descrição do Produto 3"
produto_2['valor'] = 100
produto_2['opcionais'] = ["Opcional 1",
                          "Opcional 2"]
teste.append(produto_2)

# pprint(teste)
