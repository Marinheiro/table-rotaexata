# -*- coding: utf-8 -*-
inicio_table = """
<table class="tg">
<tr>
<th id="id_nome" class="tg-0lax">Nome</th>
<th id="id_descricao" class="tg-0lax">Descrição</th>
<th id="id_valor" class="tg-0lax">Valor</th>
<th id="id_opcionais" class="tg-0lax">Opcional</th>
</tr>
"""
final_table = """</table>"""
tr_completa = """<tr>
<td class="tg-0lax" {}>{}</td>
<td class="tg-0lax" {}>{}</td>
<td class="tg-0lax" {}>{}</td>
<td class="tg-0lax">- {}</td>
</tr>
"""
tr_parcial = """
<tr>
<td class="tg-0lax">- {}</td>
</tr>
"""


def gerar_tabela(info, inicio_table=inicio_table, final_table=final_table, tr_completa=tr_completa):
    table = "{}".format(inicio_table)
    for produto in info:
        qtd_opcionais = len(produto["opcionais"])
        rowspan = """rowspan='{}'""".format(qtd_opcionais) if qtd_opcionais > 1 else ""
        table += tr_completa.format(
            rowspan,
            produto["nome"],
            rowspan,
            produto["descricao"],
            rowspan,
            produto["valor"],
            produto["opcionais"][0]
        )
        if qtd_opcionais > 1:
            for opcional in produto["opcionais"][1:]:
                table += tr_parcial.format(opcional)
    table += final_table
    return table

